Service Dependencies
====================

We would like to model services that run on top of other services. In our case we have a SecuStack offering and a managed Kubernetes running on top of SecuStack.

Our example implementation have been comitted as MR here: https://gitlab.com/gaia-x/gaia-x-core/gaia-x-self-descriptions/-/merge_requests/245

