{
    "@context": {
		"@vocab": "https://de.wikipedia.org/wiki/GAIA-X#",
        "owl": "http://www.w3.org/2002/07/owl#",
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
		"schema": "http://schema.org/",
		"edm": "http://ai4bd.com/resource/edm/",
		"vc": "https://www.w3.org/2018/credentials/v1",
		"foaf": "http://xmlns.com/foaf/",
		"ai4bd-data": "http://ai4bd.com/resource/data/",
        "vcard": "https://www.w3.org/2006/vcard/ns#",
    },
    "@id": "ai4bd-data:sd/service/aisign",
    "@type": "ServiceSelfDescription",
    "rdfs:label": {"@value":"AISIGN Service","@language":"en"},
    "describedBy": [
        {
            "@id": "ai4bd-data:sd/service/aisign/metadata",
            "@type": "MetaData",
            "hasName":"AISIGN",
            "hasVersion": "v3.0.6",
			"description": {"@value":"It provides a service to recognize handwritten content inside an image, a document part, via RESTful API. The image is send to the service, the service returns the recognized handwriting and the confidence. To handle different types of handwriting content (e.g. numerics, alphanumerics), AISIGN can be configured to use a specific model.","@language":"en"},
			"schema:dateModified":"2020-10-05T10:30:00Z",
			"hasScreenshot" : ["https://ai4bd.com/wp-content/uploads/2020/10/aisign-screen.png"],
            "providedBy": {
                "@id": "ai4bd-data:sd/org/ai4bd",
                "@type": "ServiceProvider",
                "hasName": "AI4BD Deutschland GmbH",
                "schema:address": "Gerwigstraße 6, 78120 Furtwangen",
                "schema:url": "https://www.ai4bd.com",
				"description":{"@value":"AI4BD AG is a Swiss company with headquarters in Zurich (CH) and a subsidiary - AI4BD Deutschland GmbH - in Furtwangen (DE) and Dresden (DE).  AI4BD provides unique ®Cognitive Business Robotics (CBR) coworkers, cognitive digital employees, for the Cognitive Enterprise System (CES). The Cognitive Enterprise System is a system which revolutionizes the way we live and work. CBR coworkers are cognitive digital employees who use AI to combine and apply their expertise, experience and skills as knowledge in standard software products. CBR coworkers can be used alone or in combination in all business process scenarios. CES solutions also protect evolutionary achievements and investments. Thanks to the support of CBR coworkers, creativity, innovation and human interaction can be strengthened and with the most productive and secure use of data and knowledge, globally deployed.","@language":"en"},
				"schema:logo":"https://ai4bd.org/wp-content/uploads/2016/09/final-logo-ai4bd-small.png",
				"contactTechnical" : {
					"@id": "ai4bd-data:sd/person/max-mustermann",
					"@type": "vcard:Agent",
					"hasName": "Max Mustermann",
					"vcard:hasEmail": "mailto:devops1@ai4bd.com"
				}
            }
        }
    ],
    "includes": [
        {
            "@type": "Testimonial",
            "@id": "ai4bd-data:sd/service/aisign/testimonial/deployment",
            "rdfs:label": "Deployment Testimonial",
            "includes": [
                {
                    "@type": "KubernetesDeployTarget",
                    "instruction": ["URL-to-k8s-deployment-script1","URL-to-k8s-deployment-script2"],
                    "source": {
                        "@type": "ImageSource",
                        "schema:url": "https://dockers1.ai4bd.org",
                        "hasLogin": "username4registry",
                        "hasPassword": "password4registry"
                    }
                },
                {
                    "@type": "DockerComposeDeployTarget",
                    "instruction": ["URL-to-dc-deployment-script1","URL-to-dc-deployment-script2"],
                    "source": {
                        "@type": "ImageSource",
                        "schema:url": "https://dockers1.ai4bd.org",
                        "hasLogin": "username4registry",
                        "hasPassword": "password4registry"
                    }
                }
            ],
			"requires": [
				{
					"@type": "Node",
					"contains" : {
						"@type" : "HardwareSpecification",
						"hasMinCpuCore": 1,
						"hasMaxCpuCore" : 5,
						"hasMinMemory" : "300mb",
						"hasMaxMemory" : "1gb"
					}
				}
			],
            "provenBy": [{
                "@type": ["JwtProof","Proof"],
				"@id": "http://ai4bd.com/resource/edm/proof/123",
				"vc:created": "2020-09-01T21:19:10Z",
				"vc:proofPurpose": "assertionMethod",
				"vc:verificationMethod": "https://sso.ai4bd.org/verify",
				"vc:jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5XsITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUcX16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtjPAYuNzVBAh4vGHSrQyHUdBBPM"
            }]
        },
        {
            "@type": "Testimonial",
            "@id": "ai4bd-data:sd/service/aisign/testimonial/api",
            "rdfs:label": "Service API Testimonial",
			"schema:description": {"@value":"The service is accessible after deployment based on a REST API.","@language":"en"},
            "includes": [
                {
                    "@type": "OpenAPI",
					"rdfs:label": "Service OpenAPI File",
                    "schema:url": "url-to-open-api"
                },
                {
                    "@type": "ServiceDescription",
					"rdfs:label": "Semantic Service Description",
					"schema:description":{"@value":"The service takes an input image with an handwritten text and returns the digitalized text.","@language":"en"},
                    "consumes": {
                        "@type": "Image",
						"schema:description": {"@value":"It is possible to send an PNG or JPG image. The text shuld be in one line.","@language":"en"},
                        "edm:contentType": "image/png",
                        "edm:depicts": "handwriting"
                    },
                    "produces": {
                        "@type": "Json",
						"schema:description":{"@value": "The service returns a the digital text and the confidence of the model about its correctness","@language":"en"},
                        "edm:schema": {
                            "@type": "JsonObject",
                            "edm:textField": "Predicted text",
                            "edm:confidence": "Confidence of the predicted text."
                        }
                    }
                }
            ],
            "provenBy": [
                {
                "@type": ["JwtProof","Proof"],
				"@id": "http://ai4bd.com/resource/edm/proof/123",
				"vc:created": "2020-09-01T21:19:10Z",
				"vc:proofPurpose": "assertionMethod",
				"vc:verificationMethod": "https://sso.ai4bd.org/verify",
				"vc:jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5XsITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUcX16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtjPAYuNzVBAh4vGHSrQyHUdBBPM"
            }
            ]
        },
        {
            "@type": "Testimonial",
            "@id": "ai4bd-data:sd/service/aisign/testimonial/payment",
            "rdfs:label": "Payment Model Testimonial",
            "includes": [
                {
                    "@type": "PayPerUsePaymentModel",
                    "hasAmount": "1",
                    "hasUnit": "EUR"
                }
            ],
            "provenBy": [
                {
                "@type": ["JwtProof","Proof"],
				"@id": "http://ai4bd.com/resource/edm/proof/123",
				"vc:created": "2020-09-01T21:19:10Z",
				"vc:proofPurpose": "assertionMethod",
				"vc:verificationMethod": "https://sso.ai4bd.org/verify",
				"vc:jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5XsITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUcX16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtjPAYuNzVBAh4vGHSrQyHUdBBPM"
            }
            ]
        }
    ],
    "provenBy": [
                {
                "@type": ["JwtProof","Proof"],
				"@id": "http://ai4bd.com/resource/edm/proof/123",
				"vc:created": "2020-09-01T21:19:10Z",
				"vc:proofPurpose": "assertionMethod",
				"vc:verificationMethod": "https://sso.ai4bd.org/verify",
				"vc:jws": "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5XsITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUcX16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtjPAYuNzVBAh4vGHSrQyHUdBBPM"
            }
    ]
}
