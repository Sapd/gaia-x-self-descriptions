@prefix dct: <http://purl.org/dc/terms/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix ids: <https://w3id.org/idsa/core/> .
@prefix owl:  <http://www.w3.org/2002/07/owl#> .
@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <http://schema.org/> .
@prefix vcard: <https://www.w3.org/2006/vcard/ns#> .
@prefix xsd:  <http://www.w3.org/2001/XMLSchema#> .

@prefix gax:  <https://de.wikipedia.org/wiki/GAIA-X#> .
@prefix ex: <http://example.org/> .

# Please do not enter secret details (passwords or personal contact information) as all development details might be public
ex:de_NBI_ACESeq_Service a gax:Service ;
  # FYI: Since the service requirements are very heterogeneous, we did not mark any fields as mandatory or optional yet.


  ### General details ###

  gax:hasName "de.NBI - ACESeq Service" ;
  gax:hasVersion "0.1" ; # version of this SD
  gax:providedBy ex:de_NBI ;

  gax:hasTag "Analytics" ;
  gax:hasTag "Data" ;
  gax:hasTag "Bioinformatic" ;
  gax:hasTag "WGS" ;

  # The suggested aspect ratio for the marketing image is roughly 1:2 (portrait),
  # cf. https://gitlab.com/gaia-x/gaia-x-core/gaia-x-self-descriptions/-/merge_requests/62#note_437830282
  gax:hasMarketingImage "https://www.denbi.de/images/Keyvisuals/co-dna-helix.png"^^xsd:anyURI ;

  gax:hasScreenshot [schema:contentUrl "https://i.ibb.co/t8j2D0G/screenshot-example.png"; rdfs:comment "Screenshot label 1"; ] ;
  gax:hasScreenshot [schema:contentUrl "https://i.ibb.co/TkQnmVP/screenshot-example-2.png"; rdfs:comment "Screenshot label 2"; ] ;

  # Last modification date of this service SD
  dct:modified "2020-11-16T12:00:00+01:00"^^xsd:dateTimeStamp ;
  # Optional: Specify more details instead, via: dct:modified "2020-10-30T12:00:00.000+02:00"^^xsd:dateTimeStamp ;


  ### API Details ###

  gax:hasApiType "REST";
  gax:description "This service estimates allele-specific copy numbers from WGS data" ;
  # gax:openApiDescription <https://pastebin.com/raw/9Z2V9ns4> ;

  gax:consumes [a gax:ApiDescription ;
    gax:description "WGS data in bam or fasta format." ;
    gax:hasContentType "WGS/fasta" ;
    rdfs:comment "Whole Genome Sequencing" ;
  ] ;

  gax:produces [a gax:ApiDescription ;
    gax:description "The service returns detection of ‘actionable’ mutations and copy number alterations to guide treatment decisions, accurate, tumor purity-, ploidy- and clonal heterogeneity." ;
    gax:hasContentType "application/csv" ;
    rdfs:comment "Analysis results" ;
  ] ;


  ### Deployment details ###

  gax:hasDeploymentDetails [
    gax:requires [gax:hasMinCpuCore 1; gax:hasMinMemory "4096 MB"; gax:hasUploadBandwidth "16 Mbit/s"; gax:hasDownloadBandwidth "10 Mbit/s" ;] ;

    gax:hasInstructions [gax:hasDeploymentScriptUrl "https://aceseq.readthedocs.io/en/latest/installation.html#docker-version" ;
                     gax:hasRepository [schema:url "http://bfg-nfs3.ipmb.uni-heidelberg.de"; gax:hasLogin "username"; gax:hasPassword "password"] ;
    ] ;
  ] ;


  ### Billing details ###

  gax:hasBilling [a gax:PayPerUseModel; gax:hasAmount 0.0; gax:hasUnit "EUR"] ;
  gax:hasBilling [a gax:BasicPaymentModel; gax:hasAmount 0.0; gax:hasUnit "EUR"; gax:hasFrequenceOfPayment "month"] ;
.
