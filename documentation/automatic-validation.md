# Automatic Validation

Our repository provides automatic validation of the syntax and – to a limited extent – the semantics of self-description instances against the self-description schema.  This is realized by validating the RDF graphs of the self-description instances against SHACL (W3C Shapes Constraint Language) shapes.  Our implementation uses RDF and SHACL tools in the [the CI pipeline](.gitlab-ci.yml) and particularly via [this SHACL validation python script](/ci/check_shacl.py).

## Controlled vocabularies

We define multiple [controlled vocabularies](implementation/ontology/ControlledVocabularies) in our repository. In order to include this knowledge at validation time, the [Python script adds them to the data graph](/ci/check_shacl.py#L47) at runtime. This allows proper validation of instances. 

In case an instance includes a property that is **not** in a controlled vocabulary, the validation will return an error.

## Validation errors

In case of validation errors, GitLab will automatically send an email to the author with a hint.  The following is an example of an error due to not using a controlled vocabulary:

![image.png](./image.png)

The author should then inspect the validation result from the respective job and fix the issue, cf. the example at https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/jobs/1048956285#L98 or this screenshot:

![image-1.png](./image-1.png)
